#!/usr/bin/env python3
"""Task 2 from page 41 """
import typing


def addition_3(a: typing.Union[int, float], b: typing.Union[int, float], c: typing.Union[int, float]) -> None:
    """Print sum of 3 digits."""
    print(a + b + c)


addition_3(2, 4, 2)
