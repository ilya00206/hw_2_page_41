#!/usr/bin/env python3
"""Task 3 from page 41 """


def hello_world() -> str:
    """return string Cześć świecie."""
    return str("Cześć świecie")


my_string = hello_world()
print(my_string)
