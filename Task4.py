#!/usr/bin/env python3
"""Task 4 from page 41 """
import typing


def subtract_2(a: typing.Union[int, float], b: typing.Union[int, float]) -> typing.Union[int, float]:
    """Subtract 2 digits and return a result."""
    return a - b


result = subtract_2(5, 2)
print(result)
