#!/usr/bin/env python3
"""Task 5 from page 41 """


class Car:
    """Car abstraction class."""
    pass


def new_car() -> Car:
    """Return Car object."""
    return Car()


my_car = new_car()
print(isinstance(my_car, Car))
